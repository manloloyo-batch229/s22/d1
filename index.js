

// Array Methods
// JS has built-in functions and methods for arrays

// Mutator Methods

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];

// push()
// Will add an element at the end of the array
// SYNTAX: arrayName.push()

// Adding an element in array
console.log("Current Array: ");
console.log(fruits);

let fruitsLength =  fruits.push("Mango");
console.log(fruitsLength);
console.log("Mutated array from push method");
console.log(fruits);

// Adding multiple elements to an array
fruits.push("Avocado", "Guava");
console.log("Mutated array from push method");
console.log(fruits);

// pop()
// will remove array element to the end part of the array
// pop method will see the deleted element 

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated array from pop method");
console.log(fruits);

// unshift()
// add sa front
// add element in the beginning of an array

fruits.unshift("Lime", "Banana");
console.log("Mutated array from unshift method")
console.log(fruits);

// shift()
// delete sa front
// removes an element in the beginning part of an array

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method");
console.log(fruits);

// splice()
// Simultaneously removes an element from a specified index number and adds elements
// SYNTAX -> arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);

fruits.splice(1, 2, "Lime", "Cherry");
console.log("Mutated array from splice method");
console.log(fruits);

// sort()
// Re-arranges the array elements in alphanumeric order
// A to Z
fruits.sort();
console.log("Mutated array from sort method");
console.log(fruits);


// reverse()
// Reverses order of array elements
// Z to A
fruits.reverse()
console.log("Mutated array from reverse method");
console.log(fruits);

// Non-Mutator Methods
// unlike the mutator methods, non-mutator methods cannot modify the array

let countries = ["US", "PH", "CAN" , "SG", "TH", "PH", "FR", "DE"];

// indexOf()
// Will return the index number of the first matching element
// If no element was found, JS will return -1
// Syntax -> arrayName.indexOf(searchValue, fromIndex)

let firstIndex = countries.indexOf("PH");
console.log("Result of index method: " + firstIndex);


// example of invalid entry
let invalidCountry = countries.indexOf("BR");
console.log("result of index method " + invalidCountry);

// lastIndexOf()
// Returns index number of the last matching element in an array
// Syntax -> arrayName.lastIndexOf(searchValue, fromIndex)

let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf " + lastIndex);

// Getting the index number starting from specified index
let lastIndexStart = countries.lastIndexOf("PH", 6);
console.log("Result of lastIndexOf " + lastIndexStart);

// slice()
//  Portions/slices elements from an array and return a new array
// Syntax -> arrayName.slice(startingIndex, endingIndex)

let slicedArrayA = countries.slice(2);
console.log("Result from slice method ");
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2, 4);
console.log("Result from slice method ");
console.log(slicedArrayB);

// slicing off elements starting from the last elements of an array

let slicedArrayC = countries.slice(-3);
console.log("Result from slice method ");
console.log(slicedArrayC);


// toString()
// Returns an array as a string separated by commas
// Syntax -> arrayName.toString()

let stringArray = countries.toString();
console.log("Result from toString method");
console.log(stringArray);


// concat()
// Combines two arrays and returns the combined result
// Syntax -> arrayA.concat(arrayB)

let taskArray1 = ["drink html", "eat js",];
let taskArray2 = ["inhale css", "breathe sass"];
let taskArray3 = ["Get git", "be node"];

let task = taskArray1.concat(taskArray2);
console.log("Result from concat method");
console.log(task);

// combining multiple arrays
console.log("Result from concat method");
let allTasks = taskArray1.concat(taskArray2,taskArray3);
console.log(allTasks);

// combining arrays with elements
console.log("Result from concat method");
let combinedTasks = taskArray1.concat("smell express" , "throw react");
console.log(combinedTasks);

// join()
// Returns an array as a string separated by specified separator string
//Syntax -> arrayName.join('separatorString')

let users = ["John", "Jane", "Joe", "Robert"];

console.log(users.join());
console.log(users.join(''));
console.log(users.join(' - '));

// Iteration Methods

// forEach
// Similar to a for loop that iterates on each array elements
// Syntax -> arrayName.forEach(function(indivElement)){statement}

allTasks.forEach(function(tasked){
	console.log(tasked);
});

let filteredTasks = [];

allTasks.forEach(function(task){
	if (task.length > 10) {
		filteredTasks.push(task);
	}

})
console.log("Result of forEach task")
console.log(filteredTasks);

// map()
// This is 

let numbers = [1, 2, 3, 4 ,5];
let numberMap = numbers.map(function(number){
	return number * number;
})

console.log("Original Array: ")
console.log(numbers); //not affected because of map()
console.log("Result from map method")
console.log(numberMap); // A new array is returned and stored in a variable

// map() vs forEach

let numberForEach = numbers.forEach(function(number){
	return number * number;
})
console.log(numberForEach); //undefined

// forEach(), loops over all items in the array as map(), but forEach() does not return new array.

// every()
// checks if all elements in an array meet the given condition
// Syntax -> let/const resultArray = arrayName.every(function(indivElement){condition})

let allValid = numbers.every(function(number){
	return (number < 3);
})

console.log("Result from every method")
console.log(allValid);

// some()
// checks if atleast one element in array meets the condition
// Syntax -> let/const resultArray = arrayName.some(function(indivElement){condition})

let someValid = numbers.some(function(number){
	return (number < 2);
})

console.log("Result from some method");
console.log(someValid);

// filter()
// Returns new array that contains elements which meet the fiven condition
// Return an empty array if no elements were found
// Syntax -> let/const resultArray = arrayName.filter(function(indivElement){condition})

let filterValid = numbers.filter(function(number){
	return (number < 3);
})
console.log("Result from filtered Valid");
console.log(filterValid);



// includes()
// Checks if the argument passed can be found in the array
// Syntax -> arrayName.includes(<argumentToFind>)

let products = ["Mouse", "Keyboard", "Laptop","Monitor"];

let productFound1 = products.includes("Mouse");
console.log(productFound1); // true output

let productFound2 = products.includes("Headset");
console.log(productFound2); // false output


// combined filter and include method
let filteredProducts = products.filter(function(product){
	return product.toLowerCase().includes("a");
})

console.log(filteredProducts);


// reduce()
// Evaluates elements from left to right and returns/reduces the array into a single value.

// Syntax --> let/const resultArray = arrayName.reduce(function(accumulator, currentValue){operation})


let iteration = 0;

let reducedArray = numbers.reduce(function(x, y){
	console.warn("Current Iteration: " + ++iteration);
	console.log("accumulator: " + x);
	console.log("currentValue: " + y);

	// The operation to reduce the array into single value 
	return x + y;
})
console.log("Result of reduce method: " + reducedArray);

let list = ["Hello", "Again", "World"];

let reducedJoin = list.reduce(function(x,y){
	return x + " " + y;
})

console.log("Result of reduced method: " + reducedJoin);